# SFML Starter Template

## Dependencies

Make sure you have, clang, cmake, and ninja installed or any alternative compiler

## Steps

1. Clone repository and use command `git submodule update --init --recursive` in the project directory

2. Edit CMakeLists.txt and change the project name, description, as well as the executable name

3. Create a build folder with `mkdir build` and change directory to it `cd build`

4. Use command `cmake .. -GNinja -DCMAKE_BUILD_FILE=Debug`

5. Compile executable with `ninja`

6. Lastly symlink the `compile_commands.json` file to project directory for ide integration (in root directory run) `ln -S ./build/compile_commands.json ./compile_commands.json`
